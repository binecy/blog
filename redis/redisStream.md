Redis消息流的介绍与SpringBoot上的应用

pub/sub有什么缺点？


cluster模式也无法实现分布式。


异步复制，依然存在丢失消息的风险。


不能支持大数据量

应用：任务队列可以吗


## stream
Redis 5提供了一种新的数据类型Stream，用于实现消息队列（Message Queue，MQ）。Redis Stream实现了大部分消息队列的功能，包括：
消息ID的序列化生成。
消息的阻塞和非阻塞读取。
消息的分组消费。
ACK确认机制。
除了Stream，Redis还有其他可以实现类似消息队列的机制，比如针对频道的发布/订阅（Pub/Sub），但Pub/Sub机制有个缺点就是消息无法持久化，如果出现网络断开、节点下线等情况，消息就会丢失。 
Redis Stream提供了消息的持久化和主从复制功能，可以让任意客户端访问任何时刻的数据，并且能记住每一个客户端的访问位置，从而保证消息不丢失。
本章主要分析Redis Stream的应用示例与实现原理。

### 添加、读取消息
XADD命令可以发送消息到指定Stream消息流中（如果对应消息流不存在，则Redis会先创建该消息流）：

> XADD userinfo 1620087391111-0 name a age 10
"1620087391111-0"
> XADD userinfo 1620087392222-0 name b age 13
"1620087392222-0"
> XADD userinfo 1620087393333-0 name c age 16
"1620087393333-0"
> XADD userinfo 1620087394444-0 name d age 19
"1620087394444-0"


userinfo为消息流的名称，消息由键值对组成，并且每个消息都关联了一个消息ID。消息ID的格式为<毫秒级时间戳>-<序号>。XADD命令的第3个参数传入消息ID，该参数也可以传入“*”，要求Redis自动生成消息ID，这里为了更好地辨认消息ID，传入了具体的消息ID。
使用XREAD命令可以直接读取消息流中的消息：

> XREAD COUNT 2 STREAMS userinfo 1620087391111-0
1) 1) "userinfo"
   2) 1) 1) "1620087392222-0"
         2) 1) "name"
            2) "b"
            3) "age"
            4) "13"
      2) 1) "1620087393333-0"
         2) 1) "name"
            2) "c"
            3) "age"
            4) "16"

XREAD命令最后的参数是消息ID，Redis会返回大于该ID的消息。“0-0”（或者直接使用“0”）是一个特殊ID，代表最小的消息ID，使用它可以要求Redis从头读取消息。

使用XREAD命令也可以阻塞客户端，等待消息流中接收新的消息：

> XREAD BLOCK 30000 STREAMS userinfo $
(nil)
(30.09s)

BLOCK选项指定了阻塞等待时间。“$”参数也是一个特殊ID，代表消息流当前最大的消息ID，使用它可以要求Redis读取新的消息。

### 消费组
Redis Stream借鉴了Kafka的设计，引入了消费组和消费者的概念。每个消息流中可以创建多个消费组，每个消费组可以关联多个消费者。Redis保证在一个消费组内，消息流的每一条消息都只会被消费组中的一个消费者消费。
例如，消息组中存在消费者S1、S2、S3，如果消息流中的某个消息M1被S1消费，则不会再被S2和S3重复消费。
通常可以按业务划分多个消费组，并将服务集群中的每个服务节点作为一个消费者，从而保证在一个业务中消息不会被重复消费。例如，账号系统、权益系统都可以在userinfo消息流中创建一个消费组，当收到新用户注册的消息（假如新用户注册时会发送消息到userinfo消息流中）时，它们分别为新用户创建账号、分配权益。
下面介绍消息组的使用。
首先使用XGROUP CREATE命令创建一个消费组：

> XGROUP CREATE userinfo cg1 0-0

每个消费组都维护了当前消费组最新读取的消息ID—last_delivered_id，这里将last_delivered_id初始化为0-0，要求消费组从头开始读取消息。
通过XREADGROUP命令使用消费者读取数据：

> XREADGROUP GROUP cg1 c1 COUNT 1 STREAMS userinfo >
1) 1) "userinfo"
   2) 1) 1) "1620087391111-0"
         2) 1) "name"
            2) "a"
            3) "age"
            4) "10"

XREADGROUP命令中的cg1为消费组，c1为消费者，最后的“>”参数也是特殊消息ID，使用该参数要求消费组读取大于last_delivered_id的消息。每次读取消息后，last_delivered_id都会被更新为最新读取的消息ID。



## spring应用
