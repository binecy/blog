实践篇 -- Redis客户端缓存在SpringBoot应用的探究
本文探究Redis最新特性--客户端缓存在SpringBoot上的应用。

## Redis Tracking
Redis客户端缓存机制基于Redis Tracking机制实现的。我们先了解一下Redis Tracking机制。

### 为什么需要Redis Tracking
Redis由于速度快、性能高，常常作为MySQL等传统数据库的缓存数据库。但由于Redis是远程服务，查询Redis需要通过网络请求，在高并发查询情景中难免造成性能损耗。所以，高并发应用通常引入本地缓存，在查询Redis前先检查本地缓存是否存在数据。
假如使用MySQL存储数据，那么数据查询流程下图所示。
![](https://gitee.com/binecy/blog/raw/master/images/1633933501046.png)

引入多端缓存后，修改数据时，各数据缓存端如何保证数据一致是一个难题。通常的做法是修改MySQL数据，并删除Redis缓存、本地缓存。当用户发现缓存不存在时，会重新查询MySQL数据，并设置Redis缓存、本地缓存。 
在分布式系统中，某个节点修改数据后不仅要删除当前节点的本地缓存，还需要发送请求给集群中的其他节点，要求它们删除该数据的本地缓存，如下图所示。如果分布式系统中节点很多，那么该操作会造成不少性能损耗。
![](https://gitee.com/binecy/blog/raw/master/images/1633933523750.png)

为此，Redis 6提供了Redis Tracking机制，对该缓存方案进行了优化。开启Redis Tracking后，Redis服务器会记录客户端查询的所有键，并在这些键发生变更后，发送失效消息通知客户端这些键已变更，这时客户端需要将这些键的本地缓存删除。基于Redis Tracking机制，某个节点修改数据后，不需要再在集群广播“删除本地缓存”的请求，从而降低了系统复杂度，并提高了性能。

### Redis Tracking的应用
下表展示了Redis Tracking的基本使用
![picture 1](https://gitee.com/binecy/blog/raw/master/images/1634043540579.png)  
（1）为了支持Redis服务器推送消息，Redis在RESP2协议上进行了扩展，实现了RESP3协议。HELLO 3命令表示客户端与Redis服务器之间使用RESP3协议通信。
注意：Redis 6.0提供了Redis Tracking机制，但该版本的redis-cli并不支持RESP3协议，所以这里需要使用Redis 6.2版本的redis-cli进行演示。
（2）CLIENT TRACKING on命令的作用是开启Redis Tracking机制，此后Redis服务器会记录客户端查询的键，并在这些键变更后推送失效消息通知客户端。失效消息以invalidate开头，后面是失效键数组。
上表中的客户端 client1 查询了键 score 后，客户端 client2 修改了该键，这时 Redis 服务器会马上推送失效消息给客户端 client1，但 redis-cli 不会直接展示它收到的推送消息，而是在下一个请求返回后再展示该消息，所以 client1 重新发送了一个 PING请求。

上面使用的非广播模式，另外，Redis Tracking还支持广播模式。在广播模式下，当变更的键以客户端关注的前缀开头时，Redis服务器会给所有关注了该前缀的客户端发送失效消息，不管客户端之前是否查询过这些键。 
下表展示了如何使用Redis Tracking的广播模式。
![picture 2](https://gitee.com/binecy/blog/raw/master/images/1634043569922.png)  
说明一下CLIENT TRACKING命令中的两个参数：
BCAST参数：启用广播模式。
PREFIX参数：声明客户端关注的前缀，即客户端只关注cache开头的键。

强调一下非广播模式与广播模式的区别：
非广播模式：Redis服务器记录客户查询过的键，当这些键发生变化时，Redis发送失效消息给客户端。
广播模式：Redis服务器不记录客户查询过的键，当变更的键以客户端关注的前缀开头时，Redis就会发送失效消息给客户端。

关于Redis Tracking的更多内容，我已经在新书《Redis核心原理与实践》中详细分析，这里不再赘述。

## Redis客户端缓存
既然Redis提供了Tracking机制，那么客户端就可以基于该机制实现客户端缓存了。

### Lettuce实现
Lettuce（6.1.5版本）已经支持Redis客户端缓存（单机模式下），使用CacheFrontend类可以实现客户端缓存。
```
public static void main(String[] args) throws InterruptedException {
    // [1]
    RedisURI redisUri = RedisURI.builder()
            .withHost("127.0.0.1")
            .withPort(6379)
            .build();
    RedisClient redisClient = RedisClient.create(redisUri);

    // [2]
    StatefulRedisConnection<String, String> connect = redisClient.connect();
    Map<String, String> clientCache = new ConcurrentHashMap<>();
    CacheFrontend<String, String> frontend = ClientSideCaching.enable(CacheAccessor.forMap(clientCache), connect,
            TrackingArgs.Builder.enabled());

    // [3]
    while (true) {
        String cachedValue = frontend.get("k1");
        System.out.println("k1 ---> " + cachedValue);
        Thread.sleep(3000);
    }
}
```
1. 构建RedisClient。
2. 构建CacheFrontend。
ClientSideCaching.enable开启客户端缓存，即发送“CLIENT TRACKING”命令给Redis服务器，要求Redis开启Tracking机制。
最后一个参数指定了Redis Tracking的模式，这里用的是最简单的非广播模式。
这里可以看到，通过Map保存客户端缓存的内容。
3. 重复查询同一个值，查看缓存是否生效。

我们可以通过Redis的Monitor命令监控Redis服务收到的命令，使用该命令就可以看到，开启客户端缓存后，Lettuce不会重复查询同一个键。
而且我们修改这个键后，Lettuce会重新查询这个键的最新值。

通过Redis的Client List命令可以查看连接的信息
```
> CLIENT LIST
id=4 addr=192.168.56.1:50402 fd=7 name= age=23 idle=22 flags=t ...
```
`flags=t`代表这个连接启动了Tracking机制。

### SpringBoot应用
那么如何在SpringBoot上使用呢？请看下面的例子
```
@Bean
public CacheFrontend<String, String> redisCacheFrontend(RedisConnectionFactory redisConnectionFactory) {
    StatefulRedisConnection connect = getRedisConnect(redisConnectionFactory);
    if (connect == null) {
        return null;
    }

    CacheFrontend<String, String> frontend = ClientSideCaching.enable(
            CacheAccessor.forMap(new ConcurrentHashMap<>()),
            connect,
            TrackingArgs.Builder.enabled());

    return frontend;
}

private StatefulRedisConnection getRedisConnect(RedisConnectionFactory redisConnectionFactory) {
    if(redisConnectionFactory instanceof LettuceConnectionFactory) {
        AbstractRedisClient absClient = ((LettuceConnectionFactory) redisConnectionFactory).getNativeClient();
        if (absClient instanceof RedisClient) {
            return ((RedisClient) absClient).connect();
        }
    }
    return null;
}
```
其实也简单，通过RedisConnectionFactory获取一个StatefulRedisConnection连接，就可以创建CacheFrontend了。
这里RedisClient#connect方法会创建一个新的连接，这样可以将使用客户端缓存、不使用客户端缓存的连接区分。

### 结合Guava缓存
Lettuce的StatefulRedisConnection类还提供了addListener方法，可以设置回调方法处理Redis推送的消息。
利用该方法，我们可以将Guava的缓存与Redis客户端缓存结合
```
@Bean
public LoadingCache<String, String> redisGuavaCache(RedisConnectionFactory redisConnectionFactory) {
    // [1]
    StatefulRedisConnection connect = getRedisConnect(redisConnectionFactory);
    if (connect != null) {
        // [2]
        LoadingCache<String, String> redisCache = CacheBuilder.newBuilder()
                .initialCapacity(5)
                .maximumSize(100)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<String, String>() {
                    public String load(String key) { 
                        String val = (String)connect.sync().get(key);
                        return val == null ? "" : val;
                    }
                });
        // [3]
        connect.sync().clientTracking(TrackingArgs.Builder.enabled());
        // [4]
        connect.addListener(message -> {
            if (message.getType().equals("invalidate")) {
                List<Object> content = message.getContent(StringCodec.UTF8::decodeKey);
                List<String> keys = (List<String>) content.get(1);
                keys.forEach(key -> {
                    redisCache.invalidate(key);
                });
            }
        });
        return redisCache;
    }
    return null;
}
```
1. 获取Redis连接。
2. 创建Guava缓存类LoadingCache，该缓存类如果发现数据不存在，则查询Redis。
3. 开启Redis客户端缓存。
4. 添加回调函数，如果收到Redis发送的失效消息，则清除Guava缓存。


### Redis Cluster模式
上面说的应用必须在Redis单机模式下（或者主从、Sentinel模式），遗憾的是，
目前发现Lettuce（6.1.5版本）还没有支持Redis Cluster下的客户端缓存。
简单看了一下源码，目前发现如下原因：
Cluster模式下，Redis命令需要根据命令的键，重定向到键的存储节点执行。
而对于“CLIENT TRACKING”这个没有键的命令，Lettuce并没有将它发送给Cluster中所有的节点，而是将它发送给一个固定的默认的节点（可查看ClusterDistributionChannelWriter类），所以通过StatefulRedisClusterConnection调用RedisAdvancedClusterCommands.clientTracking方法并没有开启Redis服务的Tracking机制。
这个其实也可以修改，有时间再研究一下。


### 需要注意的问题
那么单机模式下，Lettuce的客户端缓存就真的没有问题了吗？

仔细思考一下Redis Tracking的设计，发现使用Redis客户端缓存有两个点需要关注：

1. 开启客户端缓存后，Redis连接不能断开。
如果Redis连接断了，并且客户端自动重连，那么新的连接是没有开启Tracking机制的，该连接查询的键不会受到失效消息，后果很严重。
同样，开启Tracking的连接和查询缓存键的连接必须是同一个，不能使用A连接开启Tracking机制，使用B连接去查询缓存键（所以客户端不能使用连接池）。

Redis服务器可以设置timeout配置，自动超过该配置没有发送请求的连接。
而Lettuce有自动重连机制，重连后的连接将收不到失效消息。
有两个解决思路：
（1）实现Lettuce心跳机制，定时发送PING命令以维持连接。
（2）即使使用心跳机制，Redis连接依然可能断开（网络跳动等原因），可以修改自动重连机制（Lettuce的ReconnectionHandler类），增加如下逻辑：如果连接原来开启了Tracking机制，则重连后需要自动开启Tracking机制。
需要注意，如果使用的是非广播模式，需要清空旧连接缓存的数据，因为连接已经变更，Redis服务器不会将旧连接的失效消息发送给新连接。

2. 启用缓存的连接与未启动缓存的连接应该区分。
这点比较简单，上例例子中都使用RedisClient#connect方法创建一个新的连接，专用于客户端缓存。

客户端缓存是一个强大的功能，需要我们去用好它。可惜当前暂时还没有完善的Java客户端支持，本书分享了我的一些方案与思路，欢迎探讨。我后续会关注继续Lettuce的更新，如果Lettuce提供了完善的Redis客户端缓存支持，再更新本文。

关于Redis Tracking的详细使用与实现原理，我在新书《Redis核心原理与实践》做了详尽分析，文章最后，介绍一下这本书：
本书通过深入分析Redis 6.0源码，总结了Redis核心功能的设计与实现。通过阅读本书，读者可以深入理解Redis内部机制及最新特性，并学习到Redis相关的数据结构与算法、Unix编程、存储系统设计，分布式系统架构等一系列知识。
经过该书编辑同意，我会继续在个人技术公众号（binecy）发布书中部分章节内容，作为书的预览内容，欢迎大家查阅，谢谢。

书籍详情：
[京东链接](https://item.m.jd.com/product/12911621.html)   
[豆瓣链接](https://book.douban.com/subject/35560278/)


https://www.jianshu.com/p/586051c29ef2