如何系统的阅读Redis源码

为什么要学习Redis源码？
因为值得。学习Redis源码，可以获得以下3方面的知识：
（1）深入理解Redis，有助于我们正确使用Redis。
（2）通过Redis源码，学习Redis中用到的Unix编程知识，如Unix网络的编程，多线程编程，进程的内存布局、读写文件的缓存机制等等。  
理解底层的系统知识，可以帮助我们深入理解自己写的程序，知其然也知其所以然，遇到底层的问题，也有更好的解决思路。
特别是使用Java等高级语言开发的同学，理解操作系统的知识特别有必要。
（3）大量通用的程序设计思路。
如数据结构的设计、分布式存储系统的设计方案、高可用系统的设计。
这些设计方案都是非常通用的，触类旁通，可以帮助我们去理解同类系统，如MySql、Kafka、Nginx等，甚至去设计这些系统。

### 学习顺序与思路
如何学习Redis源码呢？

可能大家下载Redis源码后，面对几十个源码文件有点迷茫，但实际上阅读Redis源码并不难，按本人一些的经验，可以按以下顺序阅读源码。

阅读Redis源码也不困难，按本人一些的经验，可以按以下顺序阅读源码。


（1）学习Redis中基础的数据结构。涉及以下源码文件
字符串：sds.c、sds.h、t_string.c
列表：ziplist.h、ziplist.c、quicklist.c、t_list.c
散列：dict.h、dict.c、t_hash.c、db.c（数据库）
无序集合：t_set.c
有序集合：t_zset.c  
列举一些值得关注的问题：
ziplist：典型的时间换空间————将链表元素压缩的一个完整的内存块中，可以节省内容，减少碎片，但每次增、删元素都需要移动元素。如果ziplist很长，则每次操作代价很大。
quicklist.c：化整为零，将ziplist拆小，再用链表链接起来，减少每次操作的移动元素代价。
dict：hash算法怎么选择？hash冲突怎么解决？如何扩容？
跳表：空间换时间，平均查找和插入时间复杂度都是O(logn)，利用了概率平衡树的思想。

（2）网络模型与命令执行
Redis网络模型：ae.h、 ae.c 
IO复用：ae_epoll.c、ae_evport.c、ae_kqueue.c、ae_select.c
命令执行： networking.c、server.c、server.h

Redis的网络模型非常值得学习，
Redis使用IO复用模型构建了自己的事件驱动模式的网络模式。
（这也是Redis高性能的一个关键点）
利用IO复用模型也是很多远程网络服务的通用模式，如Nginx。
并且，Redis对不同Unix系统的IO复用API进行了封装，设计非常优雅。
我们也可以从中学习linux下epoll的使用，这对我们理解网络模式非常有帮助，如Java Nio，Netty等。
而分析Redis命令执行流程，则是让我们学习深入理解Redis的运行：当我们发送一个命令给Redis，Redis处理这个命令的完整流程。
另外，Redis协议的设计也是值得学习的：如何设计一个文本协议，支持字符串、数值、数组，甚至是流模式等多个类型？

（3）持久化与复制
RDB机制：rdb.h、rdb.c
AOF机制：aof.c
主从复制：replication.c

这部分主要学习存储系统的设计。
Redis提供两种持久化方式，都是存储系统中常用的方式：
（1）RDB，快照持久化，类似于MySql的快照备份。
（2）AOF，写命令持久化，类似于MySql的binlog。

<!-- 主从节点的数据同步，也有两种方式
（1）通过数据快照全量同步，通常用于新的从节点同步数据。
（2）通过写命令部分同步（主节点通过发送写命令给从节点执行）。通常用于网络断开后重连时同步数据。 -->

Redis通过发送写命令在主从节点间复制数据，
这是很多存储系统主从节点复制数据的方式，如MySql通过binlog复制数据。
还有一个细节就是维护已同步的偏移量。通过该偏移量才知道从节点已经同步到哪里了，主从连接断开后从哪里开始同步。

（4）分布式架构
Sentinel机制：sentinel.c
Cluster机制：cluster.h、cluster.c

这部分主要学习分布式系统的设计。
Sentinel机制可以实现Redis主从集群高可用：通过监控主节点，当节点下线后执行故障转移。典型的高可用设计方案。
<!-- 这非常常用的高可用方案，如Kafka同样通过Zookeeper（临时节点）监控集群节点，在节点下线后执行故障转移。 -->

而关于如何设计一个分布式存储系统，并具有高可用，高可扩展性？
Cluster机制提供了一个值得参考的答案。
数据分片的设计：定义16384个槽位，将键划分到不同槽位，并将不同槽位的数据存储到不同的节点实现数据分片。
数据迁移：通过迁移槽位实现。
服务发现：使用Gossip算法实现。
故障转移：与Sentinel机制类似。

如果我们对比Redis Cluster与Kafka，可以发现他们其实有一些相似点：
（1）Redis Cluster中每个主节点负责一组槽位的键，类似于Kafka中的一个分区。
（2）Redis Cluster中每个主节点需要将自己的数据复制给从节点，类似于Kafka中的leader分区将数据负责给follow分区。

Sentinel、Cluster中还使用了Raft算法，这是非常常用的分布式一致性算法，虽然Redis并没有实现完整的raft算法，不过我们也可以从redis出发，探究该算法的设计与实现。

（5）高级特性
Redis的一些高级特性：
事务：multi.c 
非阻塞删除：bio.h、bio.c 
内存管理(数据过期、淘汰)：expire.c、evict.c
Redis Stream：rax.h、rax.c、listpack.c、listpack.h、stream.h、t_stream.c
访问控制列表 ACL：acl.c
Redis Tracking：tracking.c
Lua脚本：scripting.c
Module模块：redismodule.h、module.c

这里面也有不少值得深究的点，如
Redis Stream：借鉴Kafka，实现了Redis的消息队列。
访问控制列表ACL：每个成熟的系统，权限控制都必不可少。
Redis Tracking：Redis客户端缓存的设计思想
Lua脚本、Module模块：如何实现系统的高度可扩展性？
...


### 调试源码
阅读源码，调试是必不可少的一步。
下面说一下怎么调试Redis源码。

准备环境：
Linux操作系统，我用的是Linux mint。
调试工具：Visual Studio Code

(1)安装插件：c/c++，用于编译c/c++源码
（1）使用Vs Code打开Redis源码下的src文件夹。
（2）自动生成.vscode/tasks.json，
VsCode操作：terminal -> configure task -> create task.json file from template -> other
（3）自动生成.vscode/launch.json
VsCode操作：run -> add configuration -> c++(GDB/LLDB)
（4）修改配置内容。

.vscode/tasks.json内容修改为
```
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "build",
            "type": "shell",
            "command": "make",
            "args": [
                "CFLAGS=\"-g -O0\""
            ]
        }
    ]
}
```

.vscode/launch.json内容修改为
```
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "redis debug",
            "type": "cppdbg",
            "request": "launch",
            "preLaunchTask": "build",
            "program": "${workspaceFolder}/redis-server",
            "args": [],
            "stopAtEntry": false,
            "cwd": "${workspaceFolder}",
            "environment": [],
            "externalConsole": false,
            "MIMode": "gdb",
            "setupCommands": [
                {
                    "text": "-enable-pretty-printing",
                    "ignoreFailures": true
                }
            ]
        }
    ]
}
```
（6）启动Redis，VsCode操作：run -> start debugging
等待编译完成，服务启动就可以了。

如果你独自阅读Redis感到困难，可以参考新书《Redis核心原理与实践》。



redis与分布式锁
redis-cell 限流
redis布隆过滤（布隆去重）
HyperLogLog 