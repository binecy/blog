
zookeeper启动：

docker network create pulsar-net

sudo docker run  -it --name zk1 --network pulsar-net  --network-alias zk1 bin/pulsar:2.8  /bin/bash


执行后台运行命令，这个命令是启动zookeeper：
bin/pulsar-daemon start zookeeper


2. 执行zookeeper客户端连接命令：
bin/pulsar zookeeper-shell
客户端正常连接，就算zookeeper启动好了



bin/pulsar initialize-cluster-metadata \
    --cluster pulsar-cluster-zk \
    --zookeeper 127.0.0.1:2181 \
    --configuration-store 127.0.0.1:2181 \
    --web-service-url http://127.0.0.1:8080 \
    --web-service-url-tls https://127.0.0.1:8443 \
    --broker-service-url pulsar://127.0.0.1:6650 \
    --broker-service-url-tls pulsar+ssl://127.0.0.1:6651

bin/pulsar initialize-cluster-metadata \
    --cluster pulsar-cluster-zk \
    --zookeeper IP1:2181 \
    --configuration-store IP1:2181 \
    --web-service-url http://IP1:8080,IP2:8080,IP3:8080 \
    --web-service-url-tls https://IP1:8443,IP2:8443,IP3:8443 \
    --broker-service-url pulsar://IP1:6650,IP2:6650,IP3:6650 \
    --broker-service-url-tls pulsar+ssl://IP1:6651,IP2:6651,IP3:6651


bin/pulsar zookeeper-shell进入zk控制台，通过ls /查看所有zk节点。能看到bookies，ledgers等节点，则说明初始化成功了。


bookeeper启动

sudo docker run  -it  --name pulsar1 --network pulsar-net  --network-alias pulsar1 bin/pulsar:2.8  /bin/bash

sed -i "s/zkServers=localhost:2181/zkServers=zk1:2181/" conf/bookkeeper.conf


# ip=$(ifconfig eth0|grep "inet addr"|awk '{print $2}'|awk -F: '{print $2}');sed -i "s/advertisedAddress=/advertisedAddress=$ip/" conf/bookkeeper.conf


执行初始化元数据命令，若出现提示，输入Y继续（该步骤只需在一个bookie节点执行一次，总共只需执行一次）：
bin/bookkeeper shell metaformat


在三台机器上，分别输入以下命令来以后台进程启动bookie：
bin/pulsar-daemon start bookie

验证是否启动成功：
bin/bookkeeper shell bookiesanity
出现Bookie sanity test succeeded则代表启动成功。





Broker启动


# sed -i "s/zookeeperServers=/zookeeperServers=zk1:2181/" conf/broker.conf 

# ip=$(ifconfig eth0|grep "inet addr"|awk '{print $2}'|awk -F: '{print $2}');sed -i "s/advertisedAddress=/advertisedAddress=$ip/"  conf/broker.conf

# sed -i "s/clusterName=/clusterName=pulsar-cluster-zk/" conf/broker.conf
clusterName与前面zookeeper初始化的cluster一致


在每个部署Broker的机器上，以后台进程启动broker
bin/pulsar-daemon start broker
​ 如果需要关闭broker，可使用命令

查看集群 brokers 节点情况
bin/pulsar-admin brokers list pulsar-cluster


参考
https://zhuanlan.zhihu.com/p/272551491





发送、接收
webServiceUrl=http://172.23.0.5:8080,172.23.0.4:8080,172.23.0.3:8080/
brokerServiceUrl=pulsar://172.23.0.5:6650,172.23.0.4:6650,172.23.0.3:6650/



https://huangzhongde.cn/post/Linux/Pulsar_Cluster_Deploy/


springboot:
https://www.baeldung.com/apache-pulsar



https://blog.csdn.net/weixin_47196151/article/details/110507605


生产消息
bin/pulsar-client produce \
  persistent://public/default/test \
  -n 1 \
  -m "Hello Pulsar"



消费消息
bin/pulsar-client consume \
  persistent://public/default/test \
  -n 100 \
  -s "consumer-test" \
  -t "Exclusive"


  bin/pulsar-client consume \
  persistent://public/default/test \
  -n 100 \
  -s "consumer-test2" \
  -t "Shared"