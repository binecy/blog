Apache Spark是一个围绕速度、易用性和复杂分析构建的大数据处理框架，

https://blog.csdn.net/m0_61022929/article/details/126403124


有那些对象：org.apache.spark.sql.Dataset

DataFrame

https://blog.csdn.net/qq_42346574/article/details/118972367


启动：bin/spark-shell
```
scala> var df = spark.read.textFile("/opt/spark-3.3.0-bin-hadoop3/input.txt")
df: org.apache.spark.sql.Dataset[String] = [value: string]


scala> val wordCount = df.flatMap(line => line.split(" ")).groupByKey(identity).count()
wordCount: org.apache.spark.sql.Dataset[(String, Long)] = [key: string, count(1): bigint]

scala> wordCount.coalesce(1).write.option("header", "true").csv("/opt/spark-3.3.0-bin-hadoop3/out.csv")
```

wc.take(5)

在集群下有那些进程？ 主从？
依赖ZK




 sudo docker run -it  --name=spark1 --hostname=spark1 --network=hadoop-net --network-alias=namedata  bin/spark:3.3.0 /bin/bash
sudo docker run -it  --name=spark2 --hostname=spark2 --network=hadoop-net --network-alias=namedata  bin/spark:3.3.0 /bin/bash
 sudo docker run -it  --name=spark3 --hostname=spark3 --network=hadoop-net --network-alias=namedata  bin/spark:3.3.0 /bin/bash
 


cp conf/workers.template conf/workers

yum install openssh-server


启动


sbin/start-all.sh


编写Java实例：
https://blog.csdn.net/huaicainiao/article/details/90085017

打包：
mvn  clean install 

./bin/spark-submit \
--class com.binecy.WordCount \
--num-executors 1 \
--driver-memory 512M \
--executor-memory 512M \
--executor-cores 1 \
spark-start-1.0-SNAPSHOT.jar

Spark 支持从本地文件系统中读取文件，不过它要求文件在集群中所有节点的相同路径下都可以找到。


# ./bin/spark-submit --class com.binecy.WordCount --num-executors 1 --driver-memory 512M --executor-memory 512M --executor-cores 1 spark-start-1.0-SNAPSHOT.jar







$ sudo docker cp /home/binecy/java/bin-practice/spark-start/target/spark-start-1.0-SNAPSHOT.jar spark01:/opt/spark-3.3.0-bin-hadoop3




![](../images/1686440412795.png)

spark  基于内存计算  
hadoop
storm
rdd 容错

![](../images/1686444763695.png)