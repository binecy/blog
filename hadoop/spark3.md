

https://blog.csdn.net/shaock2018/article/details/108334274

```
//引入Spark Streaming中的StreamingContext模块
import org.apache.spark.streaming._
// 创建的 Spark Context 对象为 sc ，我们需要创建本地的StreamingContext对象，第二个参数为处理的时间片间隔时间，设置为1秒
val ssc = new StreamingContext(sc, Seconds(1))

// 创建DStream，指明数据源为socket：来自localhost本地主机的9999端口
val lines = ssc.socketTextStream("localhost", 9999)

// 使用flatMap和Split对这1秒内所收到的字符串进行分割
val words = lines.flatMap(_.split(" "))

// map操作将独立的单词映射成(word，1)元组
val pairs = words.map(word => (word, 1))

// 对统计结果进行相加，得到（单词，词频）元组
val wordCounts = pairs.reduceByKey(_ + _)


wordCounts.print

// 启动Spark Streaming应用
ssc.start()

// 等待计算终止
ssc.awaitTermination()
```


nc -l -p 9999


https://study.163.com/course/courseLearn.htm?courseId=1005031005#/learn/video?lessonId=1051984903&courseId=1005031005

https://blog.csdn.net/sillyzhangye/article/details/119164722

https://blog.csdn.net/bbvjx1314/article/details/105427978

https://zhuanlan.zhihu.com/p/400611580


./bin/spark-submit \
--class com.binecy.WordCount3 \
--master spark://spark1:7077 \
--deploy-mode cluster \
--num-executors 1 \
--driver-memory 512M \
--executor-memory 512M \
--executor-cores 1 \
spark-start-1.0-SNAPSHOT.jar 



./bin/spark-submit \
--class com.binecy.KafkaDemo1 \
--master spark://spark1:7077 \
--deploy-mode cluster \
--num-executors 1 \
--driver-memory 512M \
--executor-memory 512M \
--executor-cores 1 \
spark-start-1.0-SNAPSHOT.jar 



https://blog.csdn.net/a805814077/article/details/106531020



 sudo docker run -it  --hostname=kafka1 --name=kafka1 --network=hadoop-net --network-alias=kafka1  bin/kafka:3.0 /bin/bash


 ./bin/spark-submit \
--class com.binecy.WordCount6 \
--master spark://spark1:7077 \
--deploy-mode cluster \
--num-executors 1 \
--driver-memory 512M \
--executor-memory 512M \
--executor-cores 1 \
spark-start-1.0-SNAPSHOT.jar 


https://blog.csdn.net/weixin_45737446/article/details/105628669
