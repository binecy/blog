rdd 概念
RDD：是弹性分布式数据集（Resilient Distributed Dataset）的简称，是分布式内存的一个抽象概念，提供了一种高度受限的共享内存模型。

RDD的操作有两种类型，即Transformation操作和Action操作。转换操作是从已经存在的RDD创建一个新的RDD，而行动操作是在RDD上进行计算后返回结果到 Driver。


Transformation操作都具有 Lazy 特性，即 Spark 不会立刻进行实际的计算，只会记录执行的轨迹，只有触发Action操作的时候，它才会根据 DAG 图真正执行。


https://zhuanlan.zhihu.com/p/70424613

有那些对象：org.apache.spark.sql.Dataset

DataFrame

https://blog.csdn.net/qq_42346574/article/details/118972367

Dataset、DataFrame、RDD是由区别的，但笔者还不是很理解（入门中）
暂时将它们理解为同一个东西把。

如果同样的数据都给到这三个数据结构，它们分别计算之后，都会给出相同的结果。不同的是它们的执行效率和执行方式。在后期的Spark版本中，DataSet可能会逐步取代RDD和DataFrame成为唯一的Api接口。


支持数据源：
1. 文本文件
Plain-text files

2. JSON

3. CSV
　1. Hive

　　2. JSON
JDBC/ODBC connections


当然也有一部分开源社区开发的数据源支持：

Cassandra
HBase
MongoDB
AWS Redshift
XML


脚本模式


hadoop

https://zhuanlan.zhihu.com/p/319578533




启动：bin/spark-shell
```
scala> var df = spark.read.textFile("/opt/spark-3.3.0-bin-hadoop3/input.txt")
df: org.apache.spark.sql.Dataset[String] = [value: string]


scala> val wordCount = df.flatMap(line => line.split(" ")).groupByKey(identity).count()
wordCount: org.apache.spark.sql.Dataset[(String, Long)] = [key: string, count(1): bigint]

scala> wordCount.coalesce(1).write.option("header", "true").csv("/opt/spark-3.3.0-bin-hadoop3/out.csv")
```



https://blog.csdn.net/sillyzhangye/article/details/119164722


https://blog.csdn.net/qq_36153312/article/details/110309739



pom添加引用
```
    <dependency> <!-- Spark dependency -->
      <groupId>org.apache.spark</groupId>
      <artifactId>spark-core_2.12</artifactId>
      <version>3.3.0</version>
    </dependency>

    <dependency>
      <groupId>org.apache.spark</groupId>
      <artifactId>spark-sql_2.12</artifactId>
      <version>3.3.0</version>
    </dependency>
```



```
package com.binecy;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import java.util.*;

public class WordCount2 {
    public static final String BASE_PATH = "hdfs://172.25.0.2:9000/";

    public static void main(String[] args) {

        String input_path = args[0];
        String output_path = args[1];

        // create spark_session
        SparkSession spark = SparkSession
                .builder()
                .appName("java WordCount example")
                .enableHiveSupport()
                .getOrCreate();

        // input
        Dataset<String> text = spark.read().textFile(BASE_PATH + input_path).as(Encoders.STRING());

        // 分词
        Dataset<String> words = text.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                System.out.println("line:" + s);
                return Arrays.asList(s.split(" ")).iterator();
            }
        }, Encoders.STRING());

        // 转化为<word, count>的形式
        Dataset<Row> word_count = words.groupBy("value").count().sort(functions.desc("count")).coalesce(1);

        // output
        word_count.write().mode("overwrite").csv(BASE_PATH + output_path);
    }
}
```

打包：
mvn  clean install 


./bin/spark-submit \
--class com.binecy.WordCount2 \
--master spark://172.25.0.5:7077 \
--deploy-mode cluster \
--num-executors 1 \
--driver-memory 512M \
--executor-memory 512M \
--executor-cores 1 \
spark-start-1.0-SNAPSHOT.jar \
/user/myfile/README.txt \
/user/myfile/README.txt-out


需要上传到work节点获取dfs，因为因为driver在集群中的任意一节点执行。

其他的方法：算子




mysql：
https://blog.csdn.net/yangyuguang/article/details/86555960


--- mysql
create table line_t
(
id INT(11)  UNSIGNED PRIMARY Key AUTO_INCREMENT,
context VARCHAR(1024)
);

insert into line_t(context) value( "hello java");
insert into line_t(context) value( "hello hadoop");


create table word_count_t
(
id INT(11)  UNSIGNED PRIMARY Key AUTO_INCREMENT,
word VARCHAR(64),
count int(6)
);


下载。。。包
放到了spark安装目录下的jars文件夹下面

```
scala> val mysqlDF = spark.read.format("jdbc").options(Map("url" -> "jdbc:mysql://172.25.0.2:3306/mydatabase", "driver" -> "com.mysql.cj.jdbc.Driver", "dbtable" -> "line_t", "user" -> "root", "password" -> "123456")).load()
	   
scala> mysqlDF.show()
+---+------------+
| id|     context|
+---+------------+
|  1|  hello java|
|  2|  hello java|
|  3|  hello java|
|  4|hello hadoop|
|  5|hello hadoop|
|  6| hello spark|
+---+------------+
```


pom
```
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>8.0.25</version>
    </dependency>
```

```

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.0.0</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <filters>
                <filter>
                  <artifact>*:*</artifact>
                  <excludes>
                    <exclude>META-INF/*.SF</exclude>
                    <exclude>META-INF/*.DSA</exclude>
                    <exclude>META-INF/*.RSA</exclude>
                  </excludes>
                </filter>
              </filters>
              <transformers>
                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                  <mainClass></mainClass>
                </transformer>
              </transformers>
            </configuration>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>
```

```
package com.binecy;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;

import java.util.Arrays;
import java.util.Properties;

public class MySqlWordCount {
    public static void main(String[] args) {
        SparkSession spark = SparkSession
                .builder()
                .appName("MySql WordCount")
                .enableHiveSupport()
                .getOrCreate();

        Properties prop = new Properties();
        prop.put("user", "root");
        prop.put("password", "123456");
        prop.put("driver","com.mysql.cj.jdbc.Driver");


        String url = "jdbc:mysql://172.25.0.2:3306/mydatabase?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC";
        Dataset<Row> rows = spark.read().jdbc(url,"line_t",prop).select("id","context").where("id >= 2");
        Dataset<String> words = rows.flatMap((FlatMapFunction<Row, String>) row -> {
            // 可以利用索引取值,从0开始
            String s = row.getString(1);
            System.out.println("line:" + s);

            return Arrays.asList(s.split(" ")).iterator();
        }, Encoders.STRING());

        // 转化为<word, count>的形式
        Dataset<Row> word_count = words.groupBy("value").count().sort(functions.desc("count"))
                .coalesce(1).toDF("word", "count");

        // output
        word_count.write().mode(SaveMode.Append).jdbc(url,"word_count_t",prop);
    }


}

```
todo 没有close？

打包
mvn  clean install 

提交

./bin/spark-submit \
--class com.binecy.MySqlWordCount \
--master spark://172.25.0.3:7077 \
--deploy-mode cluster \
--num-executors 1 \
--driver-memory 512M \
--executor-memory 512M \
--executor-cores 1 \
spark-start-1.0-SNAPSHOT.jar


# ./bin/spark-submit --class com.binecy.MySqlWordCount --master spark://spark1:7077 --deploy-mode cluster --num-executors 1 --driver-memory 512M --executor-memory 512M --executor-cores 1 spark-start-1.0-SNAPSHOT.jar



mysql> select * from word_count_t;
+----+--------+-------+
| id | word   | count |
+----+--------+-------+
|  1 | hello  |     5 |
|  2 | java   |     2 |
|  3 | hadoop |     2 |
|  4 | spark  |     1 |
+----+--------+-------+


gosu mysql /opt/mysql-8.0.25/bin/mysqld --defaults-file=my.cnf &