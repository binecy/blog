前面介绍了Hadoop的HDFS、Yarn、MapReduce（分布式计算框架），
那Spark又是什么呢？

### 简介
Apache Spark也是一个分布式计算框架，

不同于MapReduce将中间计算结果放入磁盘中，Spark采用内存存储中间计算结果，减少了迭代运算的磁盘IO，并通过并行计算DAG图的优化，减少了不同任务之间的依赖，降低了延迟等待时间。内存计算下，Spark 比 MapReduce 快100倍。



Spark支持Local模式、 StandAlone模式、Spark On Yarn模式部署模式

本文关注StandAlone模式、Spark On Yarn模式部署。

Spark On Yarn模式使用Yarn进行任务管理。
而StandAlone模式则自行进行任务管理（Spark还实现了任务调度能力）。

StandAlone模式使用主从架构，

将涉及以下Spark基本概念：


DAG：是Directed Acyclic Graph（有向无环图）的简称，反映RDD之间的依赖关系。


Cluster Manager：集群资源管理中心，负责分配计算资源。
    在standalone模式中即为Master主节点，控制整个集群，监控worker。在YARN模式中为资源管理器
Worker Node：工作节点，负责完成具体计算。
    从节点，负责控制计算节点，启动Executor或者Driver。

Driver Program：控制程序，负责为Application构建DAG图。
     运行Application 的main()函数    
Executor：是运行在工作节点（Worker Node）上的一个进程，负责运行Task，并为应用程序存储数据。
    执行器，是为某个Application运行在worker node上的一个进程

Spark的应用程序涉及以下概念：

Application：用户编写的Spark应用程序，一个Application包含多个Job。


Job：作业，一个Job包含多个RDD及作用于相应RDD上的各种操作。

Stage：阶段，是作业的基本调度单位，一个作业会分为多组任务，每组任务被称为“阶段”。

Task：任务，运行在Executor上的工作单元，是Executor中的一个线程。

总结：Application由多个Job组成，Job由多个Stage组成，Stage由多个Task组成。Stage是作业调度的基本单位。


![](../images/1685774141596.png)


Spark集群由Driver, Cluster Manager（Standalone,Yarn 或 Mesos），以及Worker Node组成。对于每个Spark应用程序，Worker Node上存在一个Executor进程，Executor进程中包括多个Task线程。


架构：
https://zhuanlan.zhihu.com/p/70424613
https://www.cnblogs.com/tgzhu/p/5818374.html

https://zhuanlan.zhihu.com/p/139262724

### 部署

3个机器 1个Cluster Manager，2个Worker Node

下载spark:3.3.0，解压



StandAlone模式部署：
（1）无密登录
（2）
在Cluster Manager上执行

部署：
nano conf/spark-env.sh
```
export JAVA_HOME=/usr/lib/jdk1.8.0_221
SPARK_MASTER_HOST=spark1
SPARK_MASTER_PORT=7077
```

nano conf/workers
```
spark2
spark3
```

启动

```
# ./sbin/start-all.sh
```



```
# jps
243 Jps
39 Master
```


```
# jps
73 Worker
155 Jps
```




界面：
http://172.25.0.5:8080/

在界面上可以看到。。。。 todo


提交一个任务：
./bin/spark-submit --class org.apache.spark.examples.SparkPi \
--master spark://172.25.0.5:7077 \
--deploy-mode cluster \
examples/jars/spark-examples_2.12-3.3.0.jar 1000


SparkPi是一个分布式计算 。。。的demo。



在界面上可以看到。。。。 todo

yarn：
不需要额外构建Spark集群，将Spark程序直接提交给Yarn执行。

有yarn-client和yarn-cluster两种模式，
Spark Client 和 Spark Cluster的区别:

理解YARN-Client和YARN-Cluster深层次的区别之前先清楚一个概念：Application Master。在YARN中，每个Application实例都有一个ApplicationMaster进程，它是Application启动的第一个容器。它负责和ResourceManager打交道并请求资源，获取资源之后告诉NodeManager为其启动Container。从深层次的含义讲YARN-Cluster和YARN-Client模式的区别其实就是ApplicationMaster进程的区别
YARN-Cluster模式下，Driver运行在AM(Application Master)中，它负责向YARN申请资源，并监督作业的运行状况。当用户提交了作业之后，就可以关掉Client，作业会继续在YARN上运行，因而YARN-Cluster模式不适合运行交互类型的作业
YARN-Client模式下，Application Master仅仅向YARN请求Executor，Client会和请求的Container通信来调度他们工作，也就是说Client不能离开

yarn-client的Driver程序运行在客户端，适用于交互、调试，而yarn-cluster的Driver程序运行在由ResourceManager启动的ApplicationMaster中，适用于生产环境


在spark1上传hadoop 下的etx目录到目录/opt/hadoop-3.2.2/etc/hadoop
修改/opt/hadoop-3.2.2/etc/hadoop/core-site.xml 
```
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://namenode:9000</value>
    </property>
</configuration>
```


yarn-site.xml添加如下选项：
```
<property>
	<name>yarn.nodemanager.aux-services</name>
	<value>mapreduce_shuffle</value>
</property>

<property>
	<name>yarn.resourcemanager.hostname</name>
	<value>resourcemanager</value>
</property>

```


conf/spark-env.sh添加：
HADOOP_CONF_DIR=/opt/hadoop-3.2.2/etc/hadoop

执行命令：
$ ./bin/spark-submit --class org.apache.spark.examples.SparkPi \
    --master yarn \
    --deploy-mode cluster \
    examples/jars/spark-examples_2.12-3.3.0.jar 100



./bin/spark-submit --class org.apache.spark.examples.SparkPi \
    --master yarn \
    --deploy-mode client \
    examples/jars/spark-examples_2.12-3.3.0.jar 100


![](../images/1684661104540.png)


架构：https://cloud.tencent.com/developer/article/1938056


历史服务器？

日志聚集？

standalone -client ？
 standalone-cluster？


 会有多个DriverID吗？

 PLAINTEXT://0.0.0.0:9092