


### 简介
我们知道，Hadoop 包含分布式文件系统 (HDFS) 和 MapReduce两大组件，分别完成存储与计算功能。 
上一篇文章介绍了HDFS的部署与应用，而MapReduce可以理解为一个个的用户作业（job），使用HDFS上的数据进行计算，得到结果。
由于用户作业需要放到集群的机器上执行，所以需要对用户作业、计算机资源（CPU，内存空间）进行管理，调度。
Yarn资源调度器正是完成该工作的，负责将集群中的资源分配给用户作业，并执行用户作业。
有了Yarn后，我们就可以开发一个MapReduce程序，提交给Yarn，由Yarn进程调度，执行，

![](../images/1681021719131.png)


Yarn包含以下核心组件：
ResourceManager：资源管理器，负责整个集群的资源管理和调度，即分配资源给用户作业，并管理所有的用户作业。
NodeManager：运行在集群所有节点的节点管理器，可以上报节点资源状态给管理器，
ApplicationMaster：用户作业管理器（每个用户作业都存在一个ApplicationMaster），负责申请资源，执行作业中的任务。


https://blog.csdn.net/qq_34720818/article/details/106966178

https://zhuanlan.zhihu.com/p/54192454

https://cloud.tencent.com/developer/article/1824501


![](../images/1681022169700.png)
这张图简单地标明了提交一个用户作业的流程，我们来具体说说每一步的过程。

（1）客户端向Yarn提交Application（一个用户作业，如MapReduce程序）。
ResourceManager向NodeManager通信，为该Application分配第一个容器（容器就是CPU、内存等计算机资源），并在这个容器中运行这个用户作业对应的ApplicationMaster。

（2）ApplicationMaster启动以后，对该用户作业进行拆分，拆分出具体的task（Map 任务，或Reduce 任务），这些task可以运行在一个或多个容器中。
ApplicationMaster然后向ResourceManager申请要运行任务的容器，申请到容器后，将任务分发到对应的NodeManager中的容器去运行，
（3）容器中运行的任务会向ApplicationMaster发送心跳，汇报自身情况。当所有任务运行完成后，ApplicationMaster再向 ResourceManager注销并释放容器资源。
以上就是一个用户作业的大体运行流程。

![](../images/1681022486668.png)

task在NodeManager节点上执行数据计算，所以NodeManager节点就是数据节点。注意Yarn会将程序发送到数据节点，然后在数据节点上执行计算。
这与我们常见的是程序读取数据后进行计算不同，这样设计是因为数据量巨大，程序拉取数据的成本过高，所以直接将程序发送到数据节点上执行，所谓“山不过来，我就过去”。
也正是因为这样，所以NodeManager与HDFS的DataNode必须部署在一起。

### 部署
下面介绍Yarn的部署步骤。
1. 准备3个机器，1个ResourceManager节点，2个NodeManager节点。
再强调一下，NodeManager服务需要使用HDFS中的数据进行计算，所有要与HDFS的NodeManager节点部署在一起。


2. 完成配置。
在3个节点完成以下配置：
（1）etc/hadoop/hadoop-env.sh最后添加JAVA_HOME配置：
```
export JAVA_HOME=/usr/lib/jdk1.8.0_221
```


（2）
在etc/hadoop/core-site.xml添加fs.defaultFS选项，指定HDFS的地址：
```
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://namenode:9000</value>
    </property>
</configuration>
```

yarn-site.xml添加如下选项：
```
<property>
	<name>yarn.nodemanager.aux-services</name>
	<value>mapreduce_shuffle</value>
</property>

<property>
	<name>yarn.resourcemanager.hostname</name>
	<value>resourcemanager</value>
</property>
```
yarn.nodemanager.aux-services：启动mapreduce_shuffle混洗技术
yarn.resourcemanager.hostname：指定resourcemanager的主机名

mapred-site.xml添加如下选项：
```
<property>
	<name>mapreduce.framework.name</name>
	<value>yarn</value>
</property>

<property>
  <name>yarn.app.mapreduce.am.env</name>
  <value>HADOOP_MAPRED_HOME=/opt/hadoop-3.2.2</value>
</property>
<property>
  <name>mapreduce.map.env</name>
  <value>HADOOP_MAPRED_HOME=/opt/hadoop-3.2.2</value>
</property>
<property>
  <name>mapreduce.reduce.env</name>
  <value>HADOOP_MAPRED_HOME=/opt/hadoop-3.2.2</value>
</property>
```
mapreduce.framework.name：指定执行MapReduce作业的运行时框架。属性值可以是local，classic或yarn。
yarn.app.mapreduce.am.env、mapreduce.map.env、mapreduce.reduce.env：
为MR应用程序主进程、Map程序、Reduce程序指定环境变量。/opt/hadoop-3.2.2是我的Hadoop解压路径。

修改etc/hadoop/workers，添加NodeManager主机名：
```
datanode1
datanode2
```

在sbin/start-yarn.sh、sbin/stop-yarn.sh开始位置添加如下配置：
```
YARN_RESOURCEMANAGER_USER=root
HADOOP_SECURE_DN_USER=root
YARN_NODEMANAGER_USER=root
```

3. 启动服务
（1）由于resourceManager需要发送命令到nodeManager节点上启动NodeManager服务，所以需要配置resourceManager节点到2个nodeManager节点之间免密登录。
（2）启动Yarn
```
./sbin/start-yarn.sh
```

启动成功后，可以在resourcemanager节点查看进程：
```
# jps
178 ResourceManager
```

在datanode下也可以看到对应进程：
```
# jps
99 DataNode
220 NodeManager
```


访问http://172.25.0.2:8088/cluster，可以看到如下页面：

![](../images/1680678686542.png)

另外，我们也可以手动启动Yarn。
todo
```

```

### 应用
下面给Yarn提交一个MapReduce程序。
Hadoop为我们提供了示例MapReduce程序，直接执行如下命令（Yarn使用HDFS存储自身运行数据，所有执行命令前需要启动HDFS）：
```
# ./bin/hadoop jar share/hadoop/mapreduce/hadoop-mapreduce-examples-3.2.2.jar wordcount file:///opt/hadoop-3.2.2/README.txt file:///opt/hadoop-3.2.2/README-out
```
该命令将本地（应该是说NodeManager主机本地）的README.txt文件进行分词统计。
![](../images/1680681507069.png)
从输出信息中可以看到MapReduce执行情况。

我们在某一台NodeManager主机下可以看到README-out文件夹
```
# ls README-out/
_temporary  part-r-00000
```
可见，Yarn将分词统计程序发送到某一个NodeManager主机，分词统计程序会读取该主机下的README.txt，执行分词操作，并输出结果。

通常情况下，我们不会直接使用NodeManager主机上的文件进行计算，而是使用HDFS上的文件进行计算，执行如下命令会对Hadoop的文件进行分词。
```
# ./bin/hadoop jar share/hadoop/mapreduce/hadoop-mapreduce-examples-3.2.2.jar wordcount /user/myfile/README.txt /user/myfile/README-out
```

执行成功后，我们可以在HDFS上/user/myfile/README-out目录下看到结果：
![](../images/1680682035563.png)


2. 历史服务器
Yarn控制台上看到Job列表，但每次重启，这些信息都会清空，如果要查看历史数据，就需要启动历史服务器。


（1）在resourcemanager节点编辑mapred-site.xml
```
<property>
        <name>mapreduce.jobhistory.address</name>
        <value>resourcemanager:10020</value>
</property>

<property>
        <name>mapreduce.jobhistory.webapp.address</name>
    <value>resourcemanager:19888</value>
</property>
```

（2）执行如下命令启动历史服务器
```
# sbin/mr-jobhistory-daemon.sh start historyserver
```

可以看到历史服务器进程
```
# jps
1040 Jps
497 ResourceManager
978 JobHistoryServer
```

打开历史服务器控制台
http://resourcemanager:19888/jobhistory 
可以看到所有的job历史

从Yarn控制台进去也可以。
![](../images/1684648535741.png)


3. 日志聚合


日志默认是存储在NodeMager本地的，
但是每一个Job会被分布在不同的节点上进行运行，如果要各个节点去查看MapTask或者ReduceTask的日志非常麻烦。于是YARN提供了日志聚合的能力——将每一个Job的历史日志都聚合在一起，存储在HDFS上，方便查看。

（1）在所有机器上配置yarn-site.xml
```
<property>
	<name>yarn.log-aggregation-enable</name>
	<value>true</value>
</property>

<property>
	<name>yarn.log-aggregation.retain-seconds</name>
	<value>604800</value>
</property>

<property>
    <name>yarn.log.server.url</name>
    <value>http://resourcemanager:19888/jobhistory/logs</value>
</property>
```
yarn.log-aggregation-enable：日志聚集功能使能 
yarn.log-aggregation.retain-seconds：日志保留时间设置7天
yarn.log.server.url：log server的地址，

开启日志聚集功能，需要重启NodeManager。ResourceManager、HistoryManager。

停止Yarn服务
```
# sbin/stop-yarn.sh
# sbin/mr-jobhistory-daemon.sh stop historyserver
```

开启Yarn服务
```
# sbin/start-yarn.sh
# sbin/mr-jobhistory-daemon.sh start historyserver
```

![](../images/1684650691758.png)
![](../images/1684650728677.png)
![](../images/1684650745279.png)



历史：https://baijiahao.baidu.com/s?id=1763774703360693024&wfr=spider&for=pc



关于Yarn介绍到这里，下一篇文章我们将介绍如何编写MapReduce程序。
通用框架，


yarn logs -applicationId application_1684659989850_0002	 > logs.txt



---



先说map，MapReduce中的map和Java或者是C++以及一些其他语言的map容器不同，它表示的意思是映射。负责执行map操作的机器（称作mapper）从HDFS当中拿./bin/hadoop jar share/hadoop/mapreduce/hadoop-mapreduce-examples-3.2.2.jar wordcount /user/myfile/README.txt /user/myfile/README-out到数据之后，会对这些数据进行处理，从其中提取出我们需要用到的字段或者数据，将它组织成key->value的结构，进行返回。



为什么要返回key->value的结构呢？直接返回我们要用到的value不行吗？



不行，因为在map和reduce中间，Hadoop会根据key值进行排序，将key值相同的数据归并到一起之后，再发送给reducer执行。也就是说，key值相同的数据会被同一个reducer也就是同一台机器处理，并且key相同的数据连续排列。reducer做的是通过mapper拿到的数据，生成我们最终需要的结果。

https://zhuanlan.zhihu.com/p/99913707

https://www.cnblogs.com/zsql/p/11600136.html
---


假设一个作业可以划分为多个map任务，reducer任务。





https://blog.csdn.net/weixin_52366661/article/details/120901903


http://172.25.0.5:8088/cluster/apps

![](../images/1681642393588.png)



本地运行：
https://blog.csdn.net/u012292754/article/details/81456675#:~:text=conf.set%28%22mapreduce.framework.name%22%2C%22local%22%29%3B%2F%2F%E6%9C%AC%E5%9C%B0%E6%A8%A1%E5%BC%8F%E8%BF%90%E8%A1%8Cmr%2C%E8%BE%93%E5%85%A5%E8%BE%93%E5%87%BA%E7%9A%84%E6%95%B0%E6%8D%AE%E5%8F%AF%E4%BB%A5%E5%9C%A8%E6%9C%AC%E5%9C%B0%EF%BC%8C%E4%B9%9F%E5%8F%AF%E4%BB%A5%E5%9C%A8hdfs%20%2F%2F,conf.set%28%22fs.defaultFS%22%2C%22hdfs%3A%2F%2Fnode1%3A9000%22%29%3Bconf.set%28%22fs.defaultFS%22%2C%22file%3A%2F%2F%2F%22%29%3B%201.1%20%E8%AE%BF%E9%97%AE%E6%9C%AC%E5%9C%B0