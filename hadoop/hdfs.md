

 sudo docker run -it  --hostname=namenode --name=namenode --network=hadoop-net --network-alias=namenode  bin/hadoop:3.2.2 /bin/bash
 sudo docker run -it  --hostname=datanode1 --name=datanode1 --network=hadoop-net --network-alias=datanode1  bin/hadoop:3.2.2 /bin/bash

 sudo docker run -it  --hostname=datanode2 --name=datanode2 --network=hadoop-net --network-alias=datanode2  bin/hadoop:3.2.2 /bin/bash


这本文章介绍HDFS（Hadoop DFS）的部署与应用，作为Hadoop系列的入门文章。
### 简介
HDFS是一个分布式文件系统，提供大数据（TB、PB级别的数据）存储服务。
HDFS会将一个完整的大文件平均分块存储到不同计算器上（几百上千的节点），并且读取文件时可以同时从多个主机取不同区块的文件，从而提交数据读取效率。
HDFS不支持动态改变文件内容，而是要求让文件一次写入就不做变化，要变化也只能在文件末添加内容。


HDFS采用主从（Master/Slave）结构模型，一个HDFS集群是由一个NameNode和若干个DataNode组成的。NameNode作为主服务器，管理文件系统元数据和客户端对文件的访问操作。DataNode负责以文件形式存储数据块。

NameNode为使用者提供文件视图，提供打开、关闭、重命名文件或目录等接口，也负责管理HDFS的元数据，如数据块到具体DataNode的映射。而在HDFS内部，文件被分成若干个数据块，存放在一组DataNode上。DataNode会在NameNode的统一调度下完成文件读写操作。

### 部署
介绍了HDFS的基本信息后，下面我们来部署HDFS。

1. 笔者使用的是hadoop:3.2.2进行部署，首先准备3台机器（Docker节点）：
namenode(172.25.0.2)， datanode1(172.25.0.3)、datanode2(172.25.0.4)
机器之间网络互通。



2. 在3台机器上完成如下配置。
(1)在 etc/hadoop/hadoop-env.sh中添加JAVA_HOME配置：
```
export JAVA_HOME=/usr/lib/jdk1.8.0_221
```

(2)修改etc/hadoop/core-site.xml内容如下：
```
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://namenode:9000</value>
    </property>
    
    <property>
        <name>hadoop.tmp.dir</name>
        <value>/var/hadoop</value>
    </property>
</configuration>
```
fs.defaultFS：HDFS服务的通信地址
hadoop.tmp.dir：临时文件存储目录


(3)修改etc/hadoop/hdfs-site.xml内容如下：
```
<configuration>
    <property>
        <name>dfs.replication</name>
        <value>2</value>
    </property>

     <property>
        <name>dfs.namenode.name.dir</name>
        <value>file:/usr/local/software/hadoop/hdfs/name</value>
        <final>true</final>
    </property>
    <property>
        <name>dfs.datanode.data.dir</name>
        <value>file:/usr/local/software/hadoop/hdfs/data</value>
        <final>true</final>
    </property>
    <property>
        <name>dfs.webhdfs.enabled</name>
        <value>true</value>
    </property>
    <property>
        <name>dfs.permissions.enabled</name>
        <value>false</value>
    </property>  

    <property>
        <name>dfs.blocksize</name>
        <value>1048576</value>
    </property>       
</configuration>
```
dfs.replication：数据副本数量
dfs.namenode.name.dir：namenode数据存储目录，hdfs元数据存储目录
dfs.datanode.data.dir：datanode数据存储目录，
dfs.webhdfs.enabled：是否开启hdfs的web访问接口
dfs.permissions.enabled：是否开启hdfs的权限验证（为了方便描述，我关闭了）



3. 在namenode机器上完成配置：
(1)修改etc/hadoop/workers内容如下：
```
datanode1
datanode2
```
namenode通过该配置文件，获取所有DataNode节点的主机名。

(2)在/etc/hosts添加如下内容：
```
172.25.0.3	datanode1
172.25.0.4	datanode2
```
以便namenode可以通过机器名访问datanode机器。


(3)在sbin/start-dfs.sh中添加如下内容（添加到文件开头）：
```
HDFS_DATANODE_USER=root
HADOOP_SECURE_DN_USER=root
HDFS_NAMENODE_USER=root
HDFS_SECONDARYNAMENODE_USER=root
```
允许root用户执行HDFS的命令。


(4)由于namenode需要发送命令到datanode节点上启动datanode服务，所以下面需要配置机器之间SSH免密登录。
这部分内容不介绍。
配置完成后，在namenode执行ssh命令，不需要输入密码即可登录datanode节点：
```
# ssh datanode1
```

4. 
接下来就可以启动HDFS了。
(1)执行以下命令初始化NameNode：
```
./bin/hdfs namenode -format
```
(2)执行以下命令启动HDFS
```
./sbin/start-dfs.sh
```

(3)启动成功后，在NameNode节点可以看到NameNode进程：
```
# jps
1989 SecondaryNameNode
1738 NameNode
```

在DataNode节点也可以看到DataNode进程：
```
# jps
170 DataNode
```


另外，我们也可以自行启动NameNode、DataNode服务。
在NameNode节点启动NameNode进程
```
# ./bin/hdfs --daemon start namenode
```
在DataNode启动datanode进程：
```
# ./bin/hdfs --daemon start datanode
```
由于我们是手动启动NameNode、DataNode服务，这种情况下可以不配置免密登录。


启动NameNode成功后，访问HDFS的Web页面：
http://172.25.0.2:9870/dfshealth.html


### 应用
部署成功后，我们就得到一个文件系统，接下来我们就可以使用这个文件系统了。

1. 脚本调用
（1）在NameNode节点，执行创建目录：
```
# ./bin/hdfs dfs -mkdir -p /user/myfile
```

（2）将README.txt文件上传到hdfs中：
```
# ./bin/hdfs dfs -put README.txt /user/myfile
```
在页面上可以看到上传的文件：
![](../images/1680398179912.png)


2. Java程序使用HDFS
接下来，我们在Java程序使用使用HDFS。
（1）创建一个Maven工程，添加HDFS依赖：
```
    <dependency>
      <groupId>org.apache.hadoop</groupId>
      <artifactId>hadoop-hdfs</artifactId>
      <version>3.2.2</version>
    </dependency>

    <dependency>
      <groupId>org.apache.hadoop</groupId>
      <artifactId>hadoop-common</artifactId>
      <version>3.2.2</version>
    </dependency>

    <dependency>
      <groupId>org.apache.hadoop</groupId>
      <artifactId>hadoop-client</artifactId>
      <version>3.2.2</version>
    </dependency>
```

（2）下面的程序展示了如果在HDFS中创建目录，上传文件，读取文件：
```
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import java.io.IOException;
import java.nio.charset.Charset;

public class HDFSStart {
    public static void main(String[] args) throws IOException {
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://172.25.0.2:9000");
        // 连接到HDFS
        FileSystem fs = FileSystem.get(conf);

        if (!fs.exists(new Path("/user/myfile"))) {
            fs.mkdirs(new Path("/user/myfile"));
        }

        // 本地文件路径
        Path src = new Path("/home/binecy/input.txt");

        // hdfs上传路径
        Path dst = new Path("/user/myfile/input.txt");

        // 文件上传
        fs.copyFromLocalFile(src, dst);

        FSDataInputStream hdfsInputStream =  fs.open(dst);
        IOUtils.readLines(hdfsInputStream, Charset.forName("UTF-8"))
                .forEach(System.out::println);
        // 关闭
        fs.close();
    }
}
```

关于HDFS先介绍到这里。


数据块有备份吧？

参考：https://blog.csdn.net/qq_39314099/article/details/103681298

https://blog.csdn.net/sinat_39410753/article/details/108016958


实例：https://blog.csdn.net/qq_43430810/article/details/107643696
shuffle过程：https://blog.csdn.net/weixin_44844089/article/details/117164474
https://blog.csdn.net/qq_22938671/article/details/104559708

得到了一个分布式存储系统
存储大文件 pb级别的


启动自动执行：
 service ssh start

固定ip

dfs.blocksize	


todo 
9000是什么端口？