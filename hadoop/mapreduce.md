对数据分而治之

我们已经介绍了HDFS，HDFS可以将一个大数据（）分块存储到不同计算器上（几百上千的节点），那么如何使用这些数据进行计算，得到我们想要的结果呢？
这就需要MapReduce了。
MapReduce把一个存储在分布式文件系统中的大规模数据集切分成许多独立的小的数据集（这部分工作通常通过HDFS完成），然后分配给多个map任务处理。然后map任务的输出结果会进一步处理成reduce任务的输入，最后由reduce任务进行汇总，然后上传到分布式文件系统中。


一个Map/Reduce 作业（job） 通常会把输入的数据集切分为若干独立的数据块，由 map任务（task）以完全并行的方式处理它们。框架会对map的输出先进行排序， 然后把map的输出汇总给reduce任务，reduce任务计算得到最终结果。

数据清洗
数据统计 
网站PV和UV统计


<!-- MapReduce把整个并行运算过程高度抽象到两个函数上，一个是map另一个是reduce。Map函数就是分而治之中的“分”，reduce函数就是分而治之中的“治”。

MapReduce把一个存储在分布式文件系统中的大规模数据集切分成许多独立的小的数据集，然后分配给多个map任务处理。然后map任务的输出结果会进一步处理成reduce任务的输入，最后由reduce任务进行汇总，然后上传到分布式文件系统中。

Map函数：map函数会将小的数据集转换为适合输入的<key,value>键值对的形式，然后处理成一系列具有相同key的<key,value>作为输出，我们可以把输出看做list(<key,value>)

Reduce函数：reduce函数会把map函数的输出作为输入，然后提取具有相同key的元素，并进行操作，最后的输出结果也是<key,value>键值对的形式，并合并成一个文件。 -->



典型的单词统计程序：



就是我们常见的想法：把数据分成一个个的单词（map），把相同的单词聚集到一起（shuffle  MapReduce框架完成），
统计相同单词的数量（reduce）。

hadoop读取数据，执行split，
将数据切分，
默认按行切分，

```
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

public class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        String words[] = line.split(" ");
        for (String word : words) {
            context.write(new Text(word), new IntWritable(1));
        }
    }
}
```
map方法参数
key：读取开始位置
value：行数据
context：上下文，接受map输出。
输出也是键值对，如 <hello, 1>,<hadoop, 1>等

代码很简单，就是将行数据，按单词划分。

```
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;

public class WordCountReduce extends Reducer<Text, IntWritable, Text, IntWritable> {
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int count = 0;
        for(IntWritable value : values) {
            count++;
        }
        context.write(key, new IntWritable(count));
    }
}
```
前面Hadoop会将map输出合并（同一个单词合并）转换为reduce输入，由于
key：单词，来着与map的键
values：map的同一个键的值集合，都是1

这里统计同一个单词的数量。

flu下线接下，新看一下如果运行。


主要，启动任务：
```
package com.binecy;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class MyWordCount {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        //本地测试环境，手动设置（Active NN,Active RM）配置信息
        Configuration conf = new Configuration();
//        conf.set("fs.defaultFS", "hdfs://172.25.0.4:9000");
//        conf.set("yarn.resourcemanager.hostname", "172.25.0.5");


        // 获取运行时输入的参数，一般时通过shell脚本传进来
//        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
//
//        if (otherArgs.length < 2) {
//            System.out.println("必须输入读取文件路径和输出路径");
//            System.exit(2);
//        } else {
//            System.out.println("otherArgs: " + otherArgs[0] + "  "  + otherArgs[1]);
//        }

        Job job = Job.getInstance(conf);
        job.setJarByClass(MyWordCount.class);
        job.setJobName("myWordCount");

        // 设置实现输入/输出路径
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        // 设置实现map/reduce函数的类
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReduce.class);

        // 设置reduce函数的key、value数据类型
        job.setOutputKeyClass(Text.class);
        
        job.setOutputValueClass(IntWritable.class);
        // 提交job，等待完成
        job.waitForCompletion(true);

    }
}

```


打包：
```
mvn  clean install 
```

运行：
```
./bin/hadoop jar hadoop-start-1.0-SNAPSHOT.jar   /user/myfile/README.txt /user/myfile/README-out
```

本地提交：
```
    //本地测试环境，手动设置（Active NN,Active RM）配置信息
    Configuration conf = new Configuration();
    conf.set("fs.defaultFS", "hdfs://172.25.0.4:9000");
    conf.set("yarn.resourcemanager.hostname", "172.25.0.5");



    Job job = Job.getInstance(conf);
```


分词程序为例子，介绍MapReduce处理流程。

MapReduce其实可以分为3个步骤：
map：从小的数据集中提取我们需要的内容，转换<key, value>键值对序列。在分词程序中就是对数据进行分词，输出“<单词，1>”，1代表单词数量，还没有对单词进行合并统计，所以每个单词都是1（<hello, 1>, <hadoop, 1>,<hello, 1>, <hadoop, 1>）
shuffle：将map输出的key->value结构体，按key进行分组，并将同一个key的结构体进行排序，最后将分组排序后的数据交给Reduce任务处理，所以同一个key的数据将交个同一个Reduce任务处理。
Reduce：对相同key的数据数据进行归并，得到我们的结果。在分词程序中就是对单词进行统计，得到结果（<hello, 2>, <hadoop, 2>）




shuffle是MapReduce程序的核心部分，进一步对shuffle进行说明。
shuffle分为在map端、reduce端。

map端：
map任务会输出键值对结果，
（shuffle的）map端会根据键，将数据划分到不同的分区，存储在内存缓冲器中（不同分区的数据由不同的reduce任务处理），并且同一个分区数据，map端使用键对其进行排序。
当内存中的数据达到一定阀值后，再将数据输出到本地文件，产生一个溢出文件（spill file）。
每次内存缓冲器达到溢出值，都会产生一个溢出文件，map端可以产生很多溢出文件，最后map端会对这些文件进行合并，压缩等，直到最后每个分区只有1、2个排好序的文件。


reduce端：
Yarn中的ApplicationMaster负责统筹MapReduce过程，reduce任务通过它知道从哪里获取map数据。
reduce端复制多个map端输出，将map输出文件合并为更大的，排好序的文件（类似合并排序）
注意，最后一次合并的结果并不是存储到文件中，而是直接将数据输入reduce任务处理。

![](../images/1681603267047.png)

如图中所示

![](../images/1684630244152.png)




MapReduce是一种分布式计算模型，核心在于并行计算，假设数据分布在HDFS的100个节点中，则上述步骤可以同时在100个节点上进行，“分而治之”，实现分布式计算。

shuffle是这种计算模型的核心，是可以不断优化和改进的。
在这里大做文章，
比如MapReduce利用磁盘存储数据，可支持大数据量，但也导致合并慢，性能低。如果直接在内存完成所有的shuffle操作，则可以大幅提高计算速度，Spark等框架如此实现的。



数据分布在集群机器中，分布式计算 
横向扩展能力


https://zhuanlan.zhihu.com/p/377204048#MapReduce%E7%9A%84%E5%B7%A5%E4%BD%9C%E8%BF%87%E7%A8%8B

topN：
https://blog.csdn.net/clearlxj/article/details/118607118

etl：
https://blog.csdn.net/lslslslslss/article/details/121975310


shuffle：
https://zhuanlan.zhihu.com/p/36253130
https://blog.csdn.net/kaede1209/article/details/81953262