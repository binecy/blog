1. 本地
https://www.w3cschool.cn/hbase_doc/hbase_doc-m3y62k51.html

http://www.hbase.org.cn/docs/35.html



 sudo docker run -it  --hostname=hbase --name=hbase --network=hadoop-net --network-alias=hbase  bin/hbase:2.5.4 /bin/bash

启动zk
 sudo docker run -it  --hostname=zk --name=zk --network=hadoop-net --network-alias=zk  bin/zk:3.6.3 /bin/bash

cp conf/zoo_sample.cfg  conf/zoo.cfg

./bin/zkServer.sh start


--- 

 nano conf/hbase-env.sh
```
export JAVA_HOME=/usr/lib/jdk1.8.0_221
export HBASE_MANAGES_ZK=false
```


nano conf/hbase-site.xml
```
   <property>
        <name>hbase.rootdir</name>
        <value>hdfs://namenode:9000/hbase</value>
    </property>


  <property>
    <name>hbase.cluster.distributed</name>
    <value>true</value>
  </property>
  <property>
    <name>hbase.tmp.dir</name>
    <value>./tmp</value>
  </property>
  <property>
    <name>hbase.unsafe.stream.capability.enforce</name>
    <value>false</value>
  </property>

<property>
  <name>hbase.zookeeper.quorum</name>
  <value>zk:2181</value>
</property>

<!--zookeeper数据目录-->
<property>
  <name>hbase.zookeeper.property.dataDir</name>
  <value>/root/hbase/zookeeper</value>
</property>
```


nano conf/regionservers 
```
datanode1
datanode2
```


 ./bin/start-hbase.sh

 # jps
1459 HQuorumPeer
250 NameNode
1820 Jps
509 SecondaryNameNode


# jps
1568 Jps
1329 HRegionServer
118 DataNode


访问：http://172.25.0.3:16010/master-status