应用到web开发 
如果我们向下游请求数据，那边下游就可以视为发布者 
从redis，mysql，kafka等数据源中获取数据，这些数据源都可以视为发布者

而我们需要对数据进行加工改造，我们就是一个消费者 
前端需要对数据展示，也可以视为消费者

响应式可以跟异步很好的结合 
同步，线程阻塞，等拿到数据后在处理数据 
异步响应式，给数据注册一个消费者便可以返回（阻塞），等真正拿到数据后，再找一个线程来执行消费者便可

下一步文章来看看

![](../images/binecy.png)




sql
spel
HTML
声明式事务和编程式事务

通过表达式，完成一系列命令编程工作

对命令编程简化



https://www.zhihu.com/question/22285830




操作台不仅可以操作现有的数据，还可以操作将来流过来的数据，
所以特别适合于异步

Lambda 
map.fileter.  
这里已经体现了数据流，声明式编程，数据变化

数据流可以引出几个概念
数据源
消费者


函数响应式编程






基于事件的模型    异步编程范式





 
如





声明式编程

下一篇文件看下spring flux的使用，以及比之前的异步回调函数好在哪里


-------- start
spring reactor


响应式编程概念
响应式编程是一个专注于数据流和变化传递的异步编程范式。

reactive programming is a declarative programming paradigm concerned with data streams and the propagation of change

响应式编程（reactive programming）是一种基于数据流（data stream）和变化传递（propagation of change）的声明式（declarative）的编程范式


解析一下上面的概念
声明式编程

https://www.zhihu.com/question/22285830

命令式编程
一步一步告诉计算机先做什么再做什么。
我们平时写的语言

使用表达式告诉计算机我要的结果，有计算机决定具体做法

sql
spel
HTML
声明式事务和编程式事务

数据流 变化传递
数据流就像一条车间流水线，数据在上面流动过程中，经过不同的操作台，可以被观测，被过滤，被操作（和 Java Stream 一致），或者为新的消费者与另外一条流合并为一条新的流。

lists.stream().filter(i -> i%2==0).map(i -> i * 2).toArray();

lists.stream() 流水线
filter  map  两个操纵台，他们负责处理数据 这里通过方法直接告诉计算机我们要的结果，可以理解为声明式

这里还涉及另一个概念，函数式编程，
函数式，就是将函数作为一个数据类型
作为参数，返回值，属性

注意，这里只解析最易于理解的概念
函数式编程还有很多学术性，专业性的概念，这里不一一列举






响应式编程 在上面概念加了异步，可以处理未来的数据。
比如
```
flux.futer().filter...
```
futer 从3秒后产生数据
filter 正常的声明式
最主要的是，当前线程这里不会阻塞，
等到数据产生后，在另外使用线程执行


在数据源还没有产生数据前， 我们就可以设置好消费者，等到数据产生消费者便可直接消费它。
这里类似于异步回调函数，但优于回调函数，因为他还有很多声明式编程的功能，这里下一篇文章说明。
异步观察者模式？


```java
public static void main(String[] args) throws InterruptedException {
    Flux flux = Flux.interval(Duration.ofSeconds(1));
    flux.subscribe(i -> {
        System.out.println( Thread.currentThread().getName() + " receive -> " + i);
    });

    System.out.println(Thread.currentThread().getName() + " finish");
    // 阻塞当前线程
    new CountDownLatch(1).await();
}
```
注意 subscribe后Flux才开始

背压

推/拉


没一次产生了数据，都会通知客户端，由客户端pull数据


不要小看这个异步处理，组合声明式编程，netty等异步网络框架，
下面看一下webflux

https://stackoverflow.com/questions/49951060/difference-between-flux-create-and-flux-generate

https://projectreactor.io/docs/core/release/reference/#schedulers



BaseSink 池，负责生产数据，有很多种
IgnoreSink
BufferAsyncSink
LatestAsyncSink


那么它跟其他的异步框架有什么区别呢
比如AysncTemplate
限于篇幅，下一步文章来看看


Flux.range，
fromArray等静态方法都会返回一个Flux子类
如FluxRange，FluxArray  它是一个CorePublisher


Publisher#subscribe  将Subscriber注册到Publisher，请求Publisher开始流水作业，有Flux#subscribe实现
CorePublisher#subscribe 重载，内部的subscribe方法，由Flux子类执行
Flux#subscribe
```
public final void subscribe(Subscriber<? super T> actual) {
    CorePublisher publisher = Operators.onLastAssembly(this);
    CoreSubscriber subscriber = Operators.toCoreSubscriber(actual);

    try {
        ...

        publisher.subscribe(subscriber);
    }
    catch (Throwable e) {
        Operators.reportThrowInSubscribe(subscriber, e);
        return;
    }
}
```


Subscriber#onSubscribe Publisher#subscribe中会调用该方法，
CoreSubscriber#onSubscribe CoreSubscriber也有该方法
注意，该方法参数Subscription代表了一个Subscriber与Publisher之前一次完整的订阅周期
Subscription#request 请求Publisher产生数据

Subscriber#onNext   Publisher产生数据后通过该方法发送给Subscriber


QueueSubscription ？？？

flux子类中会将CoreSubscriber转换为Subscription，如
FluxRange
```
public void subscribe(CoreSubscriber<? super Integer> actual) {
    ...
    actual.onSubscribe(new RangeSubscription(actual, st, en));
}
```
注意，RangeSubscription由FluxRange实现，也就是它是属于Publisher端的逻辑

BaseSubscriber#onSubscribe
```
public final void onSubscribe(Subscription s) {
    if (Operators.setOnce(S, this, s)) {
        try {
            hookOnSubscribe(s);
        }
        catch (Throwable throwable) {
            onError(Operators.onOperatorError(s, throwable, currentContext()));
        }
    }
}
```

BaseSubscriber#hookOnSubscribe调用Subscription#request方法，向Publisher请求数据

Publisher生成数据后，

RangeSubscription#request -> slowPath -> Subscriber#onNext

这里，Flux不同的子类对应不同的Publisher，不同的Subscription，
而不同BaseSubscriber则是不同的消费逻辑
我们可以在BaseSubscriber#hookOnNext中实现自己的逻辑


而更简单的Lambda
```

```
这里Lambda对应接口为Consumer
不过是Spring生成了一个LambdaSubscriber，
LambdaSubscriber就是在onSubscribe，onNext都调用Consumer#accept，以调用逻辑

下面再看一下Sink
Sink就是一个数据池，Sink#next，向池中投入一个数据
通过Sink，可以由我们更灵活的生成数据


为什么要异步呢
因为它跟netty这些异步网络通信框架可以很好的结合


create是将Sink交给了用户，
用户可以多线程


create(Consumer<? super FluxSink<T>> emitter)
只在构建时调用一次emitte，通过emitte，我们可以将其他数据源绑定到FluxSink中
create是一个push模式
由用户主动触发调用Sink#next

generate(Consumer<SynchronousSink<T>> generator)
每次request都会generator生成数据
pull模式，有消费者主动request


声明式

doOnNext




线程与调度器


Schedulers.immediate()
Schedulers.single()
Schedulers.boundedElastic()
Schedulers.parallel()


可以处理未来的数据。
比如
```
flux.futer().filter...
```
futer 从3秒后产生数据
filter 正常的声明式
最主要的是，当前线程这里不会阻塞，
等到数据产生后，在另外使用线程执行


在数据源还没有产生数据前， 我们就可以设置好消费者，等到数据产生消费者便可直接消费它。
这里类似于异步回调函数，但优于回调函数，因为他还有很多声明式编程的功能，这里下一篇文章说明。
异步观察者模式？



