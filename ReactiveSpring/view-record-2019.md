平安(4-1) ：
HashMap/线程池/jvm锁/内存回收
threadLoad有什么特定，用于什么场景，可能导致问题（内存泄露）

为什么选用kafka
kafka消息阻塞或丢失
接触过什么熔断框架

分布式事务实现 （业内还没有很完善的解决方案，一般也没有这种业务需求）

redis 几主几从
redis主从读取都是从主服务器读取吗
redis解决单点缓存问题
redis主从怎么同步

仓库占用防超卖

springMvc/strust有什么区别
springMvc怎么分发(DispatcherServlet)

dubbo客户端怎么选择一台机器
dubbo有那些层次

使用redis/zk实现分布式锁

项目特色
对当前项目有什么改进方案
怎么设计一个秒杀系统
项目开发遇到什么特别的问题

HttpServlet的生命周期
spring中bean的生命周期

阿里：
dubbo与springcloud对比


中信：
联合索引a,b,c 
where a > 10 and b = 3 and c = 5，使用了哪些索引
where a = 10 and b < 3 and c = 5，使用了哪些索引


redis多主多从


redis哨兵中，leader挂了，怎么选举
kafka中怎么选举leader

线上应用变慢，怎么排查问题

怎么处理突发的流量

10亿个数（内存加载不下），取最大的1000个数  
最大堆的时间复杂度是多少 nlgn


记忆深刻的解决线上问题


有赞(4-10)：
thread调用两次start
对象什么时候进入老年区
1. 对象年龄超过15
2. Young GC时  Survivor 放不下
3. 大对象直接进入老年区

2. gc root对象
Class
Thread - 活着的线程
Stack Local - Java方法的local变量或参数
JNI Local - JNI方法的local变量或参数
JNI Global - 全局JNI引用
Monitor Used - 用于同步的监控对象
Held by JVM - 用于JVM特殊目的由GC保留的对象


3. spring中循环引用解决，properties加载机制，

3. dubbo中SPI与JDK SPI区别， @adaptive 和 @activate的使用

4. 有几种线程池，用的是哪种，为什么用这个线程池
newWorkStealingPool/newFixedThreadPool/newSingleThreadExecutor/newCachedThreadPool

拒绝策略
1、直接丢弃（DiscardPolicy）
2、丢弃队列中最老的任务(DiscardOldestPolicy)。
3、抛异常(AbortPolicy)
4、将任务分给调用线程来执行(CallerRunsPolicy)。

5. hashmap多线程cpu问题

ReentrantLock怎么实现公平锁和非公平锁

分库唯一id怎么做


库存防超卖
一致性hash的缺点
cms 怎么做压缩
netty的工作模式
线程池  怎么设置coreSize maxSize
redis实现分布式锁 一般用什么数据结构
dubbo的层次  为什么使用dubbo不用http
java直接内存好处
mysql innodb用什么结构？讲一下b+树，聚值索引和非聚值索引区别

有赞（4-16）
redis 为什么可以实现分布式锁       单线程
redis 单线程有什么优点
redis 过期的key怎么存放 用队列还是hash
怎么实现过期
redis 的网络层怎么做的
怎么理解多路复用
linux epoll和selector有什么区别


spring aop原理
spring有什么代理方法  
jdk代理和cglib代理有什么区别
通过哪个扩展点实现 beanPostprocess

kafka怎么实现消息顺序


bigo:
dubbo 有什么协议
dubbo 线程模型
dubbo 协议 和 hession协议有什么区别
dubbo处理异常 通过字符串把堆栈传给客户吗？

jvm启动常用什么参数


zk 保证了CAP 哪些特性
zk什么情况不能提供服务
zk脑裂

线程池 有什么设计模式

netty 处理粘包


http 1 和http1.1有什么区别

MySQL Innordb默认的事务级别是什么，通过什么实现   mvcc
java client 和server的区别*

bigo 面试官很友好，问题比较开放，更类似于技术探讨。 时长达3小时，只记得一些印象比较深的题目了。
1. 缓冲和数据库数据一致性    回答：先更新数据库，再失效缓冲
2. 设计一个dns系统，上亿的数据量，怎样设计缓冲/数据结构，尽量保证更新数据时，数据一致性已经高效查询数据*
回答：hash表  jvm/redis缓冲
3. 一个数据系统，数据存入后5秒自动失效，上亿的数据量，怎样设计数据结构保证数据存入，查询，失效？
回答：hash表，时间轮

4. 手写代码
求和最大的子数组
前序遍历二叉树 不用递归   提示 用栈 
